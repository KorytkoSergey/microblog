from django.urls import path, include
from user_card import views
from django.conf import settings
from django.conf.urls.static import static
app_name = 'user_card'
urlpatterns = [
    path('index/', views.index, name='index'),
    path('user_card/<slug:slug>/', views.UserCard.as_view(), name='user'),
    path('post_list/<str:author>', views.author_posts, name='author_posts'),
    path('page404/', views.pass_page)
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)