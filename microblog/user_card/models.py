from django.db import models
from django.utils import timezone
from django.utils.text import slugify

class User(models.Model):
    username = models.CharField('Имя', max_length=255, unique=True)
    data_create_user = models.DateField(auto_now_add=True)
    data_birth = models.DateField(blank=True, null=True)
    slug = models.SlugField(max_length=255)

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.username)
        super().save(*args, **kwargs)
    def __str__(self):
        return self.username

class Themes(models.Model):
    name = models.CharField('Тематика', max_length=255)
    def __str__(self):
        return self.name

class Post(models.Model):
    post_id = models.CharField(max_length=255, unique=True)

    class Status(models.TextChoices):
        DRAFT = 'DF', 'Draft'
        PUBLISHED = 'PB', 'Published'
    title = models.CharField('Заголовок', max_length=255)
    slug = models.SlugField(max_length=255)
    author = models.ForeignKey(User,
                               on_delete=models.CASCADE,
                               related_name='blog_posts')
    body = models.TextField()
    publish = models.DateTimeField(default=timezone.now)
    create = models.DateTimeField(auto_now_add=True)
    update = models.DateTimeField(auto_now=True)
    status = models.CharField(max_length=2,
                              choices=Status.choices,
                              default=Status.DRAFT)
    themes = models.ManyToManyField(Themes, blank=True)
    def __str__(self):
        return self.title


class UserID(models.Model):
    number = models.CharField(max_length=255, unique=True)
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='userid')

