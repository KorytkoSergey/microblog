from django.shortcuts import render
from django.views.generic import ListView, DetailView
from user_card import models




class TitleMixin:
    title = None

    def get_title(self):
        return self.title

    def get_context_data(self, **kwargs):
        context= super().get_context_data(**kwargs)
        context['title']= self.get_title()
        return context

def index(request):
    users = models.User.objects.all()
    return render(request=request, template_name='microblog/index.html', context={'users': users})

def pass_page(request):
    return render(request=request, template_name='microblog/page404.html')

class UserCard(TitleMixin, DetailView):
    model = models.User
    template_name = 'microblog/user_card.html'
    context_object_name = 'user'
    slug_url_kwarg = 'slug'
    title = 'Страница автора'

def author_posts(request, author):
    author_posts = models.Post.objects.filter(author__username=author)
    return render(request=request, template_name='microblog/post_list.html', context={
        'author_posts': author_posts,
        'author': author
    })